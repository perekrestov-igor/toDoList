$(document).ready(function(){

    //start preparatory operations
    function after_load() {
        $(".complited h1").after("<div class='placeholder'>complited tasks will be moved here</div>");
        $(".canceled h1").after("<div class='placeholder'>canceled tasks will be moved here</div>");
        $(".in_work h1").after("<div class='placeholder'>new task will be moved here after adding</div>");

        var toDoList_exist = localStorage.getItem('toDoList_exist');
        if(!toDoList_exist) {
            toDoList_exist = true;
            localStorage.setItem('toDoList_exist', toDoList_exist);
            toDoList_task_counter = 0;
            localStorage.setItem('toDoList_task_counter', toDoList_task_counter);
        } else {
            //$(".placeholder").css("display","none");
            var task_number = Number(localStorage.toDoList_task_counter);
            for (var i = 1; i <= task_number; i++) {
                var task_obj = JSON.parse(localStorage.getItem("toDoList_task_number_" + i));
                if (task_obj == null){
                    continue;
                }
                if ($("." + task_obj.section + " .placeholder").css("display") == "block") {
                    $("." + task_obj.section + " .placeholder").css("display","none");
                }
                create_new_task_module(task_obj,"after_load",i);
            }
        }

        $("#blank").after(
            "<div id='confirmation_delete' title='Removal task!'> " +
                "<p><span class='ui-icon ui-icon-alert ui-icon-in-modal-window'></span>" +
                    "Are you sure you want to delete the task?" +
                "</p>" +
            "</div>"
        );
        $("#blank").after(
            "<div id='warning_empty_title' title='Field title isn`t filled!'> " +
                "<p><span class='ui-icon ui-icon-alert ui-icon-in-modal-window'></span>" +
                    "It is necessary to fill the field title." +
                "</p>" +
            "</div>"
        );
        $("#blank").after(
            "<div id='warning_editing_mode' title='Edit mode is active!'> " +
                "<p><span class='ui-icon ui-icon-alert ui-icon-in-modal-window'></span>" +
                    "You first need to finish editing." +
                "</p>" +
            "</div>"
        );
        $("#warning_editing_mode, #warning_empty_title, #confirmation_delete").dialog({
            resizable: false,
            height:140,
            modal: true,
            autoOpen: false
        });
        $("#confirmation_delete").dialog({
            buttons: {
                "Delete task": function() {
                    $( this ).dialog( "close" );
                    task_button_delete_click();
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
        $("#warning_empty_title, #warning_editing_mode").dialog({
            buttons: {
                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
    }
    after_load();
    //end preparatory operations

    //start constants and variables
    var $target_closed_module = [];
    var important = false;
    var str_blank_task_title = "type your task's title here";
    var str_blank_task_comment = "type your task's comment here";
    var time_slide_effect = 500;//ms
    //end constants and variables


    //start general settings
    $(".task_comment").hide();
    $(document).tooltip({
        show: { delay: 800 }
    });
    //end general settings


    //start add_task_form
    function filling_add_task_form() {
        $(".new_task_title").trigger("focusin").trigger("focusout");
        $(".new_task_comment").trigger("focusin").trigger("focusout");
    }

    $(".datepicker_start").datepicker();
    $(".datepicker_end").datepicker();

    $(".new_task_title")
        .focusin(function() {
            if ($(this).val() === str_blank_task_title) {
                $(this).removeClass("tip_style").val("");
            }
        })
        .focusout(function() {
            if($(this).val() === "")
                $(this).val(str_blank_task_title).addClass("tip_style");
        });
    $(".new_task_comment").val(str_blank_task_comment)
        .focusin(function() {
            if ($(this).val() === str_blank_task_comment) {
                $(this).removeClass("tip_style").val("");
            }
        })
        .focusout(function() {
            if($(this).val() === "")
                $(this).val(str_blank_task_comment).addClass("tip_style");
        });

    function task_impotant_mark(h,direct) {
        if (this != window) {
            $target = $(this);
        } else {
            $target = h;
        }

        if ( $target.css("background-image").slice(-19,-13) !== "cd0a0a" || direct) {
            $target.css("background-image", "url(./images/jquery-ui/ui-icons_cd0a0a_256x240.png)");
        } else {
            $target.css("background-image", "url(./images/jquery-ui/ui-icons_222222_256x240.png)");
        }
    }
    $(".add_task .ui-icon-star").click(task_impotant_mark);

    function create_new_task_module(new_task, calling, task_number) {
        var $new_task_module = $("#blank").children(".task_module").clone();
        $new_task_module.find(".task_title").html(new_task.title);
        if (new_task.comment === str_blank_task_comment) {
            $new_task_module.find(".task_comment").html("...You didn`t type comment for this task.");
            $new_task_module.find(".task_comment").addClass("tip_style");
        } else {
            $new_task_module.find(".task_comment").html(new_task.comment);
        }
        $new_task_module.find(".task_date_start").html(new_task.data_start);
        $new_task_module.find(".task_date_end").html(new_task.data_end);

        if (calling == "add new task"){
            $new_task_module.css("display", "none");
            $new_task_module.find(".ui-icon-star").css("background-image", $(".add_task .ui-icon-star").css("background-image"));
        } else if(calling == "after_load") {
            if (new_task.important_flag) {
                $new_task_module.find(".ui-icon-star").css("background-image", "url(./images/jquery-ui/ui-icons_cd0a0a_256x240.png)");
            }
        }

        var section = new_task.section;
        //$("." + section + " .placeholder").after($new_task_module);
        $("." + section + " h1").next().after($new_task_module);
        //if(new_task.title == "2"){
        //    alert("!");
        //}

        if (calling == "add new task") {
            $(".add_task .new_task_title").val("");
            $(".add_task .new_task_comment").val("");
            filling_add_task_form();
            $(".datepicker_start").datepicker("setDate", "").val("date start");
            $(".datepicker_end").datepicker("setDate", "").val("date end");
            task_impotant_mark($(".add_task .ui-icon-star"), true);
            task_impotant_mark($(".add_task .ui-icon-star"));
            $new_task_module.data("toDoList_task_number",localStorage.toDoList_task_counter);
        } else if(calling == "after_load") {
            $new_task_module.data("toDoList_task_number",task_number);
        }
        if (calling != "after_load") {
            var direction = effect_direction(section);
            var was_placeholder_shown = hide_placeholder(section);
            setTimeout(function () {
                console.log(new_task.title);
                $new_task_module.show("slide", {direction: direction}, time_slide_effect * 3);
            }, ((time_slide_effect * 3) + 100) * was_placeholder_shown);
            //$new_task_module.data("toDoList_task_number",task_number);
        }
        $new_task_module.click(click_on_buttons);
    }

    $(".button_add_new_task").click(function() {
        var new_task = {};
        var $add_task = $(this).parents(".add_task");
        new_task.title = $add_task.find(".new_task_title").val();
        if (new_task.title == str_blank_task_title) {
            $("#warning_empty_title").dialog("open");
            return;
        }
        new_task.comment = $add_task.find(".new_task_comment").val();
        var $datepicker_start = $(".datepicker_start");
        new_task.data_start = read_date($datepicker_start);
        var $datepicker_end = $(".datepicker_end");
        new_task.data_end = read_date($datepicker_end);
        new_task.important_flag = $(".add_task .ui-icon-star").css("background-image").slice(-19,-13) === "cd0a0a";
        new_task.section = "in_work";

        var toDoList_task_counter = localStorage.getItem('toDoList_task_counter');
        var task_number_in_storage = Number(toDoList_task_counter) + 1;
        localStorage.setItem('toDoList_task_counter',task_number_in_storage);
        localStorage.setItem("toDoList_task_number_" + task_number_in_storage, JSON.stringify(new_task));

        create_new_task_module(new_task,"add new task");
    });
    //end add_task_form


    //start common functions
    function effect_direction (section) {
        var direction;
        if(section == "in_work") {
            direction = "right";
        } else {
            direction = "left";
        }
        return direction;
    }


    function show_placeholder_if_section_empty(section) {
        if($("."+section).find(".task_module").length === 0) {
            var direction = effect_direction (section);
            setTimeout(function() {
                $("."+section+" .ui-effects-wrapper").remove();
                $("."+section+" .placeholder").show( "slide", { direction: direction }, time_slide_effect * 3 );
             }, 100);
            return true;
        }
        return false;
    }

    function hide_placeholder(section) {
        if($("."+section+" .placeholder").css('display') === "block") {
            var direction = effect_direction (section);
            $("."+section+" .placeholder").hide("slide", { direction: direction }, time_slide_effect * 3 );
            return true;
        }
        return false;
    }

    function hide_task_comment(target) {
        var $task_module = $(target).parents(".task_module")
        var $task_comment = $task_module.find(".task_comment");
        if($task_comment.css('display') === "block") {
            $($task_comment).slideUp();
            $task_module.find("span.ui-icon-zoomout").removeClass("ui-icon-zoomout").addClass("ui-icon-zoomin");
            return true;
        }
        return false;
    }

    function is_edit_mode(target){
        if($(target).parents(".task_module").find("input").length != 0) {
            return true;
        }
        return false;
    }

    function show_warning_if_editing_mode_active(target) {
        if(is_edit_mode(target)) {
            $("#warning_editing_mode").dialog("open");
            return true;
        }
    }

    function read_date($datepicker) {
        var date = $datepicker.datepicker('getDate');
        function format(x) {
            if(x < 10) {
                return "0" + x;
            }
            return "" + x;
        }
        var str_data;
        if ($datepicker.val() == "date start" || $datepicker.val() == "date end" || $datepicker.val() == "didn't set") {
            str_data = "didn't set";
        } else {
            str_data = "" + format(date.getMonth() + 1) + "/" + format(date.getDate()) + "/" + date.getFullYear();
        }
        return str_data;
    }

    function change_name_section_in_storage($task_module,section) {
        var task_number_in_storage = $task_module.data("toDoList_task_number");
        var task_obj = JSON.parse(localStorage.getItem("toDoList_task_number_" + task_number_in_storage));
        task_obj.section = section;
        localStorage.setItem("toDoList_task_number_" + task_number_in_storage, JSON.stringify(task_obj));
    }

    function delete_task_from_storage($task_module) {
        var task_number_in_storage = $task_module.data("toDoList_task_number");
        localStorage.removeItem("toDoList_task_number_" + task_number_in_storage);
    }

    function change_task_fields_in_storage($task_module) {
        var task_number_in_storage = $task_module.data("toDoList_task_number");
        var task_obj = JSON.parse(localStorage.getItem("toDoList_task_number_" + task_number_in_storage));

        var $this_title = $task_module.find(".task_title");
        var $this_comment = $task_module.find(".task_comment");
        var $task_important_mark = $task_module.find(".ui-icon-star");
        var $task_date_start = $task_module.find(".task_date_start");
        var $task_date_end = $task_module.find(".task_date_end");
        task_obj.title = $this_title.text();
        task_obj.comment = $this_comment.text();
        task_obj.data_start = $task_date_start.text();
        task_obj.data_end = $task_date_end.text();
        if ($task_important_mark.css("background-image").slice(-19, -13) == "cd0a0a") {
            task_obj.important_flag = true;
        }
        localStorage.setItem("toDoList_task_number_" + task_number_in_storage, JSON.stringify(task_obj));
    }
    //end common functions

    //start ".task_module" button`s methods
    function click_on_buttons() {
        $target = $(event.target);
        if ($target.is("span")) {
            $target = $target.parent();
        }
        var button_class = $target.attr("class");
        switch (button_class) {
            case "task_button_edit":
                task_button_edit_click($target);
                break;
            case "task_button_cancel":
                task_button_cancel_click($target);
                break;
            case "task_button_done":
                task_button_done_click($target);
                break;
            case "task_button_delete":
                show_confirmation_delete($target);
                break;
            case "task_button_backInWork":
                task_button_backInWork_click($target);
                break;
            case "task_button_showComment":
                task_button_showComment_click($target);
                break;
        }
    }

    function task_button_edit_click(target) {
        //target = this;
        var $task_module = $(target).parents(".task_module");
        var $this_title = $task_module.find(".task_title");
        var $this_comment = $task_module.find(".task_comment").eq(0);
        var $task_important_mark = $task_module.find(".ui-icon-star");
        var task_module_color = $task_module.css("background-color");
        var $task_date_start = $task_module.find(".task_date_start");
        var $task_date_end = $task_module.find(".task_date_end");
        var $task_date_start_datepicker, $task_date_end_datepicker;
        if (!is_edit_mode(target)) {
            //title
            var str_title_input = "<input type='text' class='' value='" + $this_title.text() + "'/>";
            $this_title.after(str_title_input);
            $this_title.next().focus();
            $this_title.css("display", "none");
            //comment
            var is_hidden = hide_task_comment(target);
            setTimeout(function() {
                var tag_comment = "<textarea type='text' class='task_comment' style='display: none'></textarea>";
                var str_comment = $this_comment.text();
                $this_comment.after(tag_comment).val(str_comment);
                $this_comment.next().val(str_comment);
                $this_comment.slideUp(time_slide_effect);
                $this_comment.next().slideDown(time_slide_effect);
            }, time_slide_effect * is_hidden);
            //button edit: highlight and change icon
            $(target).css("background-color", "#eeeebb");
            $(target).find(".ui-icon-pencil").removeClass("ui-icon-pencil").addClass("ui-icon-circle-check");
            //other buttons: remove effects
            $task_module.find(".task_button_cancel").css("background-color", task_module_color).css("cursor", "default");
            $task_module.find(".task_button_done").css("background-color", task_module_color).css("cursor", "default");
            $task_module.find(".task_button_showComment").css("background-color", task_module_color).css("cursor", "default");
            //add method to task_important_mark
            $task_important_mark.click(task_impotant_mark);
            //date
            var str_date_start = $task_date_start.text();
            $task_date_start.hide();
            $task_date_start.after("<input type='text' class='task_date_start' readonly>");//str_date_start
            $task_date_start_datepicker = $task_date_start.next();
            $task_date_start_datepicker.datepicker();
            $task_date_start.next().datepicker();
            var str_date_end = $task_date_end.text();
            $task_date_end.hide();
            $task_date_end.after("<input type='text' class='task_date_end' readonly>");//str_date_end
            $task_date_end_datepicker = $task_date_end.next();
            $task_date_end_datepicker.datepicker();
            if(str_date_start !== "didn't set") {
                var date_start = new Date(Number(str_date_start.slice(-4)), Number(str_date_start.slice(0, 2)) - 1, Number(str_date_start.slice(3, 5)));
                $task_date_start_datepicker.datepicker('setDate', date_start);
            } else {
                $task_date_start_datepicker.val(str_date_start);
            }
            if(str_date_end !== "didn't set") {
                var date_end = new Date(Number(str_date_end.slice(-4)), Number(str_date_end.slice(0, 2)) - 1, Number(str_date_end.slice(3, 5)));
                $task_date_end_datepicker.datepicker('setDate', date_end);
            } else {
                $task_date_end_datepicker.val(str_date_end);
            }
        } else {
            //title
            $this_title.text($this_title.next().val());//html
            $this_title.next().remove();
            $this_title.css("display", "block");
            //comment
            $this_comment.html($this_comment.next().val());//html
            $this_comment.next().slideUp(time_slide_effect);
            setTimeout(function() {
                $this_comment.next().remove();
            }, time_slide_effect);
            //button edit: unhighlight and change icon
            $(target).css("background-color", "");
            $(target).find(".ui-icon-circle-check").removeClass("ui-icon-circle-check").addClass("ui-icon-pencil");
            //other buttons: return effects
            $task_module.find(".task_button_cancel").css("background-color", "").css("cursor", "");
            $task_module.find(".task_button_done").css("background-color", "").css("cursor", "");
            $task_module.find(".task_button_showComment").css("background-color", "").css("cursor", "");
            //remove method task_important_mark
            $task_important_mark.unbind('click');
            //date
            str_date_start = read_date($task_date_start.next());
            str_date_end = read_date($task_date_end.next());
            $task_date_start_datepicker = $task_module.find("input.task_date_start");
            $task_date_end_datepicker = $task_module.find("input.task_date_end");

            $task_date_start.text(str_date_start).show();
            $task_date_end.text(str_date_end).show();

            $task_date_start_datepicker.remove();
            $task_date_end_datepicker.remove();

            change_task_fields_in_storage($task_module);
        }
    }

    function task_button_cancel_click(target) {
        if(show_warning_if_editing_mode_active(target)) return;
        var $task_module = $(target).parents(".task_module");
        var is_hidden = hide_task_comment(target);
        setTimeout(function() {
            hide_placeholder("canceled");
            $task_module.hide( "slide", { direction: "right" }, time_slide_effect * 3, function() {
                $(".canceled .placeholder").after($task_module);
                $(".canceled .placeholder").next().show( "slide", { direction: "left" }, time_slide_effect * 3 );
                show_placeholder_if_section_empty("in_work");
            } );
        }, time_slide_effect * is_hidden);
        change_name_section_in_storage($task_module, "canceled");
    }

    function task_button_done_click(target) {
        if(show_warning_if_editing_mode_active(target)) return;
        var $task_module = $(target).parents(".task_module");
        var is_hidden = hide_task_comment(target);
        setTimeout(function() {
            hide_placeholder("complited");
            $task_module.hide( "slide", { direction: "right" }, time_slide_effect * 3, function() {
                $(".complited .placeholder").after($task_module);
                $(".complited .placeholder").next().show( "slide", { direction: "left" }, time_slide_effect * 3 );
                show_placeholder_if_section_empty("in_work");
            });
        }, time_slide_effect * is_hidden);

        change_name_section_in_storage($task_module, "complited");
    }

    function task_button_delete_click() {

        while ($target_closed_module.length){
            var $target = $target_closed_module.shift();
            var $target_task_module = $target.parents(".task_module");
            var section = $target_task_module.parent()[0].className;/////////
            var is_hidden = hide_task_comment($target_task_module);//$target_closed_module
            setTimeout(function () {
                $target_task_module.hide("slide", {direction: "left"}, time_slide_effect * 3, function () {
                    delete_task_from_storage($target_task_module);
                    $target_task_module.remove();
                    //show_placeholder_if_section_empty("complited");
                    //show_placeholder_if_section_empty("canceled");
                    if (section == "complited") {
                        show_placeholder_if_section_empty("complited");
                    } else if (section == "canceled") {
                        show_placeholder_if_section_empty("canceled");
                    }
                });
            }, time_slide_effect * is_hidden);
        }
    }
    function show_confirmation_delete($target) {
        $target_closed_module.push($target);
        console.log($target_closed_module.length);
        $("#confirmation_delete").dialog("open");
    }

    function task_button_backInWork_click(target) {
        var $target_task_module = $(target).parents(".task_module");
        var section = $target_task_module.parent()[0].className;
        var is_hidden = hide_task_comment(target);
        setTimeout(function() {
            hide_placeholder("in_work");
            $target_task_module.hide("slide", { direction: "left" }, time_slide_effect * 3, function() {
                $(".in_work .placeholder").after($target_task_module);
                $target_task_module.show("slide", { direction: "right" }, time_slide_effect * 3);
                if (section == "complited") {
                    show_placeholder_if_section_empty("complited");
                } else if (section == "canceled") {
                    show_placeholder_if_section_empty("canceled");
                }
            });
        }, time_slide_effect * is_hidden);

        change_name_section_in_storage($target_task_module, "in_work");
    }

    function task_button_showComment_click(target) {
        if(show_warning_if_editing_mode_active(target)) return;
        $(target).parents(".task_module").find(".task_comment").slideToggle(time_slide_effect);//this
        $(target).find("span.ui-icon-zoomin").toggleClass(function () {//this
            if ($(target).find("span.ui-icon-zoomin").is('.ui-icon-zoomin')) {//this
                return 'ui-icon-zoomout';
            } else {
                return 'ui-icon-zoomin';
            }
        });
    }
    //end ".task_module" button`s methods
});

